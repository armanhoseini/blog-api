const ResponseHandler = require('../shared/utilities/response-handler');
const DateTimeHelpers = require('../shared/utilities/date-time-helper');
const _enums = require('../shared/variables/enums.json');


class BaseService {
    constructor (repository) {
        this.repository = repository;
        this.responseHandler = new ResponseHandler();
        this.enums = _enums;
    }
    async find(id){
        let entity = await this.repository.find(id);
        if(entity){
            entity.createdAt = entity.createdAt?DateTimeHelpers.convertGregorianToJalaliDateTime(entity.createdAt):null;
            this.responseHandler.setSuccessfulStatus(entity,'');
        }else{
            this.responseHandler.setFailureStatus(null,
                __('messages.itemNotFound', {item: __(`entities.${this.repository.entity}`)}),
                this.enums.STATUS_CODES.NOT_FOUND)
        }
        return this.responseHandler;
    }

}

module.exports = BaseService;