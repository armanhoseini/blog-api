const dbConfig = require('../shared/config/db');
class BaseRepository {
    constructor(entity) {
        this.db = dbConfig.db;
        this.pgp = dbConfig.pgp;
        this.entity = entity;
        this.applicationDBPrefix = process.env.APPLICATION_DB_PREFIX;
    }

    getAll() {

    }

    find(id) {

    }

}

module.exports = BaseRepository;