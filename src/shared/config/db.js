const blueBirdPromise = require('bluebird');
const pgPromise = require('pg-promise');

let options = {
    promiseLib:blueBirdPromise
};

const pgp = pgPromise(options);
let connectionConfiguration = {
    host:process.env.POSTGRES_HOST_ADDRESS,
    port:process.env.POSTGRES_PORT_NUMBER,
    database:process.env.POSTGRES_DB_NAME,
    user:process.env.POSTGRES_USER,
    password:process.env.POSTGRES_PASSWORD,
    poolSize:process.env.POSTGRES_POOL_SIZE,
};


let db = pgp(connectionConfiguration);

module.exports = {
    pgp,
    db
};