const ResponseHandler = require('../../shared/utilities/response-handler');
const _enums = require('../../shared/variables/enums.json');
const responseHandler = new ResponseHandler();
const logStream = require('../config/global').logStream();

module.exports = (app)=> {
    //<editor-fold desc="Catch 404 error">
    app.use(function (req, res, next) {
        const err = new Error('Not Found');
        err.status = 404;
        next(err);
    });
    //</editor-fold>

    //<editor-fold desc="Handle Errors on development">
    if (process.env.NODE_ENV === 'development') {
        app.use(function (err, req, res, next) {

            err.message = err.message + '\n\n\n\n\n\n';
            logStream.error(err);

            err.status = err.status || 500;
            switch (err.status) {
                case 500:
                    responseHandler.setFailureStatus({
                        error: err.stack
                    }, 'API Error', _enums.STATUS_CODES.API_PROBLEM);
                    res.status(200).send(responseHandler);
                    break;
                default:
                    res.status(err.status).send(err.message);
                    break;
            }

        });
    }
    //</editor-fold>

    //<editor-fold desc="Handle Errors on production">
    if (process.env.NODE_ENV === 'production') {
        app.use(function (err, req, res, next) {
            err.status = err.status || 500;
            err.message = err.message + '\n\n\n\n\n\n';
            logStream.error(err);
            //save error in db or file or cloud
            switch (err.status) {
                case 500:
                    responseHandler.setFailureStatus(null, 'API Error', _enums.STATUS_CODES.API_PROBLEM);
                    res.status(200).send(responseHandler);
                    break;
                default:
                    res.status(err.status).send(err.message);
                    break;
            }
        });
    }
    //</editor-fold>
};