require('dotenv').config({ path: './.env' });
const bunyan = require('bunyan');



const log = {
    development:()=>{
        return bunyan.createLogger({name:'Transport Service(DEVELOPMENT)',level:'debug'});
    },
    production:()=>{
        return bunyan.createLogger({name:'Transport Service(PRODUCTION)',level:'info'});
    }
};

const logStream = bunyan.createLogger({
    name: 'parentAPI',
    streams: [{
        type: 'rotating-file',
        path: '/data/ftp/files/log/parent.log',
        period: '1d',   // daily rotation
        count: 3        // keep 3 back copies
    }]
});

let splitedDirName = __dirname.split('/');

let srcName = '/'+splitedDirName[splitedDirName.length-3];
let rootName = '/'+splitedDirName[splitedDirName.length-4];
const appAddress = __dirname.substr(0,__dirname.indexOf(srcName)+srcName.length);//search last index of!
const rootAddress = __dirname.substr(0,__dirname.indexOf(rootName)+rootName.length);//search last index of!

module.exports = {
    appPath:appAddress,
    rootPath:rootAddress,
    log:(env)=>{
        if(env) return log[env]();
        return log[process.env.NODE_ENV || 'development']();
    },
    logStream:(env)=>{
        return logStream;
    }
};