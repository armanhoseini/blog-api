const path = require('path');
const logger = require('morgan');
const bodyParser = require('body-parser');
const i18n = require('i18n');
const globalConfig = require('./global');
const expressValidator = require('express-validator');
const persianJs = require('persianjs');



module.exports = (app)=> {
    if (process.env.NODE_ENV === 'development') {
        app.use(logger('dev'));
    }
    app.use(bodyParser.json({limit: '500kb'}));
    app.use(bodyParser.urlencoded({extended: false}));

    i18n.configure({
        locales: ['en', 'fa'],
        directory: `${globalConfig.rootPath}/src/shared/locales`,
        defaultLocale: 'fa',
        register: global,
        objectNotation: true
    });
    app.use(i18n.init);
    app.use(expressValidator({
        customSanitizers: {
            correctPersianNumbers: value => {
                return persianJs(value).persianNumber().toString()
            }
        },
        customValidators:{
        }
    }));

    // var viewPath = path.join(globalConfig.appPath, 'views');
    // app.set('views', viewPath);
    // app.set('view engine', 'pug');


};