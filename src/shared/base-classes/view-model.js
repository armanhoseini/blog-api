class BaseViewModel {
    constructor(properties, sourceObject) {
        let _self = this;
        for (let key of properties) {
            _self[key] = sourceObject[key];
        }
    }
}

module.exports = BaseViewModel;