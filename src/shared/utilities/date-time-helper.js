const moment = require('moment-jalaali');
const persianJs = require('persianjs');

module.exports = class DateTimeHelpers {
    static convertGregorianToJalaliDateTime (strGregorianDateTime) {
        return moment(strGregorianDateTime).format('jYYYY/jMM/jDD HH:mm:ss');
    }

    static separateJalaliDateParts (strStandardJalaliDate, separator = '/') {
        return `${strStandardJalaliDate.substr(0, 4)}${separator}${strStandardJalaliDate.substr(4, 2)}${separator}${strStandardJalaliDate.substr(6, 2)}`;
    }

    static hasStandardDateFormat (str) {
        return str.match(/^1[0-9]{3}\/[0-9]{2}\/[0-9]{2}/);
    }

    static getTodayJalaali (useSeparator = true, convertToEnglishNumbers = true) {
        let nowDate;

        if (useSeparator) {
            nowDate = moment().format('jYYYY/jMM/jDD')
        } else {
            nowDate = moment().format('jYYYYjMMjDD')
        }

        if (!convertToEnglishNumbers) {
            return nowDate
        } else {
            return persianJs(nowDate).toEnglishNumber().toString()
        }
    }

    static jDaysBetween(start,end){
        let jStart = moment(start, 'jYYYY/jM/jD');
        let jEnd = moment(end, 'jYYYY/jM/jD');
        return jEnd.diff(jStart,'day');
    }

    static addDaysToGregorian(jalaaliDate,days){
        let jDate = moment(jalaaliDate, 'jYYYY/jM/jD');
        jDate.add(days,'days');
        return jDate.format('YYYY-MM-DD')
    }

    static getTodayGregorain(){
        return moment().format('YYYY-MM-DD');
    }
};
