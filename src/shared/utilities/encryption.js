const bCrypt = require('bcrypt');
const saltRound = 10;

module.exports = {
    generatePasswordHash:(password) => {
        const salt = bCrypt.genSaltSync(saltRound);
        return bCrypt.hashSync(password, salt);
    },
    comparePasswordWithHashed:(password, passwordHash)=> {
        return bCrypt.compareSync(password, passwordHash);
    }
};