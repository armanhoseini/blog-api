const jwt = require('jsonwebtoken');

module.exports = {
    generateUserToken(user){
        //generate jwt from user:
        const tokenTime = 60 * 60 * 24 * 30; // token time for 30 days
        const SECRET = process.env.JWT_SECRET;
        return jwt.sign({
            id: user.id,
            mobileNumber: user.mobileNumber,
            userId:user.userId
        }, SECRET, {
            expiresIn: tokenTime
        });
    }
};