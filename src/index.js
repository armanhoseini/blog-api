const express = require('express');

const appConfig = require('./shared/config/app');
const errorConfig = require('./shared/config/error');
const routeConfig = require('./shared/config/route');

const app = express();

appConfig(app);
routeConfig(app);
errorConfig(app);


module.exports = app;